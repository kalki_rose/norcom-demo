var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({
    // Explicitly include non-gulp plugins
    pattern: ['gulp-*', 'gulp.*', 'streamqueue']

});

var fs = require('fs');

// Load Config
var config = require( '../gulp-config.js' );

module.exports = function(cb) {
    var sourcefile = config.dev.assets_dir + '/' + config.dev.source_filename;
    // Test requires that the source file has been built, check for existance
    fs.stat(sourcefile, function(err, stat) {
        if (!err) {
            // Load the generated source file from the dev-build-js task
            gulp.src(sourcefile)

            // Append the test files
            .pipe(plugins.addSrc.append(config.test_files.js ))

            .pipe(plugins.karma({
                configFile: config.test_files.karma_config.dev_filename,
                action: 'run'
            })
            .on('error', function(err) {
                //cb();
            }));

            cb();
        } else {
            console.error('Exception: No source file found.');
            cb();
        }
    });
};


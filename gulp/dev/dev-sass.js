var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({});

var config = require( '../gulp-config.js' );

module.exports = function() {
  return gulp.src(config.app_files.sass)
  	.pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass({outputStyle: 'compressed'}).on('error', plugins.sass.logError))
    .pipe(plugins.sourcemaps.write('./maps'))
    .pipe(gulp.dest('tmp/assets'));
};


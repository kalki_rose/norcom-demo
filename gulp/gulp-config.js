/**
* This file/module contains all configuration for the build process.
*/

var base_dir = 'src/client';
var vendor_dir = 'node_modules';
var moduleName = 'norcom-demo';

module.exports = {

    // Base src directory
    base_dir: base_dir,

    dev: {
        // Temporary Build Directory
        build_dir: 'tmp',
        // Temporary Assets Directory
        assets_dir: 'tmp/assets',
        //File name of the outputted javascript
        source_filename: 'source.js',
        // Filename of the main less file
        less_filename: 'main.less',
        // Filename of the processed less > css
        style_output_filename: 'main.css',
        // Directory for js and css maps
        map_dir: 'maps',
        // String used to determine browser support for auto-prefixer
        browser_support: 'last 2 versions'
    },

    // Main app config
    app: {
        // Template filename for html2js
        template_name: moduleName + '.templatesApp'
    },


    // Gulp Task Config
    gulp: {
        dev: {
            task_folder: 'gulp/dev'
        }
    },


    /*
     * All Source Files
     */
    app_files: {
        app: [
            base_dir + '/index.js'
        ],
        assets: [
            base_dir + '/assets/**/*'
        ],
        js: [
            base_dir + '/components/**/*.js',
            '!' + base_dir + '/**/*.spec.js'
        ],
        atpl: [
            base_dir + '/components/**/*.tpl.html'
        ],
        sass: [
            base_dir + '/index.scss'
        ],
        sass_files: [
            base_dir + '/**/*.scss'
            ],
        index_html: [
            base_dir + '/index.html'
        ],
        templates: [
            base_dir + '/**/*.tpl.html'
        ],
        tests: [
            base_dir + '/**/*.spec.js'
        ]
    },

    /**
    * This is a collection of files used during testing only.
    */
    test_files: {
        karma_config: {
            dev_filename: './gulp/dev-karma.conf.js',
            release_filename: './gulp/release-karma.conf.js'
        },
        js: [
            vendor_dir + '/angular-mocks/angular-mocks.js',
            base_dir + '/**/*.spec.js',
        ],
        protractor: [
            'protractor/**/*.spec.js'
        ]
    },

    /**
     * All Vendor Files
     */
    vendor_files: {
        js: [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/angular/angular.js',
            'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
            'node_modules/moment/moment.js'
        ],
        css: [
            'node_modules/bootstrap/dist/css/bootstrap.min.css'
        ],
        assets: [
        ]
    },
};

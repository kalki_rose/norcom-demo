var gulp = require('gulp');

// Load Config
var config = require( './gulp/gulp-config.js' );

var plugins = require('gulp-load-plugins')({
    // Explicitly include non-gulp plugins
    pattern: ['gulp-*', 'gulp.*', 'run-sequence']

});

require('gulp-load-tasks')(config.gulp.dev.task_folder);

/**
 * Development Single Run Build
 */
gulp.task('dev-build', function(cb) {
    plugins.runSequence(['dev-build-js',  'dev-copy-assets', 'dev-sass'], ['dev-inject-sources'], cb);
});

/**
 * Development Watch
 */
gulp.task('dev-watch', ['dev-watch-js', 'dev-watch-index', 'dev-watch-assets', 'dev-watch-sass', 'dev-watch-templates']);


/** 
 * Testing
 */
 gulp.task('test', function(cb) {
 	plugins.runSequence('dev-clean', 'dev-build', 'dev-test', cb);
 });

/*
 * Default Gulp Task: Development
 * Process:
 * - Clean build directory
 * - Initial develpoment build to temporary folder
 * - Parrallel Watches for source directory
 */
gulp.task('default', function (cb) {
    plugins.runSequence('dev-clean', 'dev-build', 'dev-watch', cb);
});


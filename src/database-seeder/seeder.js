/**
 * Load input file and seed ElasticSearch database
 */

var fs = require('fs');
var path = require('path');
var elasticsearch = require('elasticsearch');
var lineReader = require('line-reader');

// Initialise Client
var client = new elasticsearch.Client({
    host: 'localhost:9200'
});

// Check Elastic cluster is healthy
var ping = function(cb) {
    client.ping({
        requestTimeout: 30000,
        }, function (error) {
            if (error) {
            console.error('Search cluster is down!')
            cb(error);
        } else {
            console.log('Search cluster is healthy');
            cb();
        }
    });
}

// Total count of indexed items
var totalCount = 0;

// Bulk index items
var indexAll = function(data, last, cb) {
    if (data.length) {
        var dataArray = [];

        // Build request body
        for (var i = 0; i < data.length; i++) {
            obj = data[i];
            if (obj._id && obj._id.$oid) {
                var id = obj._id.$oid;
                delete obj._id;

                // Insert header of record
                dataArray.push({index: {_index: 'article', _type: 'document', _id: id}});

                // Insert record
                dataArray.push(obj);
            }
         };

         var request = {
             body: dataArray
         }

        // Async bulk request
        client.bulk(request, function( err, response  ) {
            if( err ) {
                console.log("Failed", err);
            } else {
                totalCount += response.items.length;
            }

            // If response form last record set, call back to main function
            if (last) {cb()};
        });
    }
}

var initClient = function(filePath, cb) {
    var count = 0;
    var dataArray = [];

    lineReader.eachLine(filePath, function(line, last) {
        var data = JSON.parse(line)
        dataArray.push(data);

        // Chunk lines to reduce memory on load of large file.
        if (++count > 500  || last) {
            count = 0;
            indexAll(dataArray, last, cb);
        }
    });
}

var main = function() {
    ping(function(err) {
        if (!err) {
            // Sample file path
            // TODO: update to take command line argument
            var filePath  = path.resolve(__dirname, '../../sample-input/enron.json');

            initClient(filePath, function(err) {
                console.log('Indexed Items: ', totalCount);
            });
        }
    })
}();

var express = require("express");
var app = express();

//Creating Router() object
var router = express.Router();

var http = require('http').Server(app);
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');

var ElasticSearch = require('./components/elasticsearch');

var clientPath = '/../../tmp';
var basePath = '/../../';
var assetPath = clientPath + '/assets';

/**
 * Homepage
 */
router.get('/', function(req, res){
    res.sendFile(path.join(__dirname + clientPath + '/index.html'));
});

/**
 * Search request
 */
router.get('/search', function(req, res) {
    ElasticSearch.query(req.query, function(data, err) {
        var response = (err) ? err : data;

        res.json(response);
        res.end();
    });
});

// Rest Demonstration
router.get('/api/article/:documentId', function(req, res) {
    ElasticSearch.get(req.params.documentId, function(data, err) {
        var response = (err) ? err : data;

        res.send(response);
        res.end();
    });
});

app.use(bodyParser.json());
app.use('/assets', express.static(__dirname + assetPath ));
app.use("/", router);

var port = process.env.VCAP_APP_PORT || 1337;

app.listen(port, function(){
  console.log('listening on 127.0.0.1:1337');
});

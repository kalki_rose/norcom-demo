var fs = require('fs');
var path = require('path');

var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'localhost:9200',
  // log: 'trace'
});

/**
 * Ping Elastic search cluster to check for health
 */
var ping = function() {
    client.ping({requestTimeout: 30000}, function (error) {
        if (error) {
            console.error('Search cluster is down!', error);
        } else {
            console.log('Search cluster is healthy');
        }
    });
};

/**
 * Query elastic search for search term, limit, and page
 * @param  {Object}   req Express request object
 * @param  {Function} cb  callback function for completion
  */
var query = function(req, cb) {
    var params = {
        size: req.limit || 20,
        from: req.page || 0,
        q: req.text
    };

    client.search(params).then(function (body) {
        var response = {
            total: body.hits. total,
            documents: body.hits.hits
        };

        cb(response);
    }, function (error) {
        console.trace(error.message);
        cb(null, error);
    });
}

/**
 * Query Elastic search for a single document by id
 * @param  {String}   id The document id
 * @param  {Function} cb
 */
var get = function(id, cb) {
    var params = {
        index: 'article',
        type: 'document',
        id: id
    };

    client.get(params).then(function (body) {
        cb(body);
    }, function (error) {
        console.trace(error.message);
        cb(null, error);
    });
}

ping();

module.exports = {
    ping: ping,
    query: query,
    get: get
}

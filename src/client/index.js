/**
 * Norcom Demo Application
 * Angular entry point
 */
angular.module('norcom-demo', [
    'norcom-demo.templatesApp',
    'norcom-demo.search',
])

.controller( 'AppCtrl', function() {});

// Bootstrap Angular with Sever Config
angular.element(document).ready(function () {
    angular.bootstrap(document, ['norcom-demo']);
});

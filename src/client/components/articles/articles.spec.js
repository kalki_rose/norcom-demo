describe('norcom-demo.articles', function() {

    var $httpBackend, $rootScope, authRequestHandler;

    beforeEach(function() {
        module('norcom-demo.articles');
    });

    beforeEach(inject(function($injector) {
        // Set up the mock http service responses
        $httpBackend = $injector.get('$httpBackend');

        // backend definition common for all tests
        authRequestHandler = $httpBackend.when('GET', 'search')
                            .respond(mock);

        // Get hold of a scope (i.e. the root scope)
        $rootScope = $injector.get('$rootScope');
    }));

    afterEach(function() {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
   });

    describe('ArticlessService', function() {
        beforeEach(function() {
            inject(function(_ArticlesService_) {
                ArticlesService = _ArticlesService_;
            });
        });

        it('should initialise the article service', function() {
            expect(ArticlesService).toBeDefined();
        });

        it('should make ajax request', function() {
            $httpBackend.expectGET('search?limit=10&page=0&text=test').respond(mock);
            ArticlesService.search('test', 10, 0);
            $httpBackend.flush();
        });

        it('should contain articles', function() {
            $httpBackend.expectGET('search?limit=10&page=0&text=test').respond(mock);
            ArticlesService.search('test', 10, 0);
            $httpBackend.flush();

            var articles = ArticlesService.getArticles();
            expect(articles.length).toBeGreaterThan(0);
        });

        describe('ArticleClass', function() {
            var article, mockData;

            beforeEach(function() {
                inject(function(_ArticlesService_) {
                    ArticlesService = _ArticlesService_;
                });
            });

            beforeEach(function() {
                $httpBackend.expectGET('search?limit=10&page=0&text=test').respond(mock);
                ArticlesService.search('test', 10, 0);
                $httpBackend.flush();

                var articles = ArticlesService.getArticles();

                article = articles[0];

                mockData = mock.documents[0];
            });

            it('should initialise', function() {
                expect(article).toBeDefined();
            });

            it('should provide article id', function() {
                var id = article.getId();

                expect(id).toEqual(mockData._id);
            });

            it('should provide article subject', function() {
                var subject = article.getSubject();

                expect(subject).toEqual(mockData._source.subject);
            });
        });
    });
});

var mock = {
    total: 1,
    documents: [
        {
            _id: '001122',
            _source: {
                sender: 'Test Sender',
                subject: 'Test Subject',
                date: new Date()
            }
        }
    ]
};

angular.module('norcom-demo.articles', [])
.service('ArticlesService', [ '$http', function($http) {
    var service = {};

    // Initialise service variables
    service.articles = [];
    service.total = [];
    service.articleUrl = 'search';

    /**
     * Article class constructor
     * @param {Object} article Raw article data from server
     */
    var Article = function(article) {
        this.article = article;
        this.id = this.article._id || '';
        this.sender = this.article._source.sender || '';
        this.subject = this.article._source.subject || '';
        this.date = this.article._source.date || '';
    };

    /**
     * Get article name
     * @return {String} Unboxed article name || empty
     */
    Article.prototype.getId = function() {
        return this.article._id || '';
    };

    /**
     * Get article sender
     * @return {String} Unboxed article sender || empty
     */
    Article.prototype.getSender = function() {
        return this.article._source.sender || '';
    };

    /**
     * Get article subject
     * @return {String} Unboxed article subject || empty
     */
    Article.prototype.getSubject = function() {
        return this.article._source.subject || '';
    };

    /**
     * Get article date formatted
     * @return {String} Unboxed article subject || empty
     */
    Article.prototype.getDate = function() {
        return output = moment(new Date(this.article._source.date)).format("MMMM Do YYYY");
    };

    /**
     * Get article text
     * @return {String} Unboxed article text || empty
     */
    Article.prototype.getText = function() {
        return this.article._source.text || '';
    };

    /**
     * Async search request for articles match text string
     * @param  {String} text  text to search on
     * @param  {Integer} page current page of results to request for paging
     * @param  {Integer} limit number of results to fetch per page
     * @return {Promise} A promise that will resolve with the results from the server.
     *                     in the happy case, this will be an array of Article objects.
     */
    service.search = function(text, limit, page) {
        // Initialise request params
        var config = {
            params: {
                limit: limit || 10,
                page: page || 0,
                text: text
            }
        };

        return $http.get(service.articleUrl, config).success( function(response) {
            service.total = response.total;
            service.articles = response.documents.map(function(article) {
                return new Article(article);
            });

            return service.articles;
        });
    };

    service.getArticles = function() {
        return this.articles;
    };

    service.getTotal = function() {
        return this.total;
    };

    return service;
}]);

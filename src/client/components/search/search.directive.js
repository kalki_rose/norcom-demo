/**
 * Norcom Demo Search Directive
 */

angular.module('norcom-demo.search', ['norcom-demo.articles'])
.directive('norcomSearch', [function() {
    return {
        templateUrl: 'search/search.tpl.html',
        restrict: 'E',
        replace: true,
        controllerAs: 'view',
        controller: function($scope, ArticlesService) {
            var view = $scope;

            // Initialise Variables
            view.searchForm = {
                text: '',
                size: 10
            };
            view.articles = [];
            view.searchedOnce = false;
            view.pagination = {
                current: 0
            };

            view.orderField = 'id';
            view.orderType = {
                id: true,
                subject: true,
                date: true,
                sender: true
            };

            /**
             * Main search function
             * @param  {Integer} page Page number used for pagination. Default: 0
             */
            view.search = function(page) {
                // System error, requesting page out of range
                if ((view.pagination.pages && page >= view.pagination.pages) || page < 0) {
                    return;
                }

                // Set default pagination if none supplied
                view.pagination.current = page || 0;

            	ArticlesService.search(view.searchForm.text, view.searchForm.size, page).then(function(response) {
                    // Initial search check for zero results
                    view.searchedOnce = true;

                    view.articles = ArticlesService.getArticles();

                    view.pagination.total = ArticlesService.getTotal();
                    view.pagination.size = view.searchForm.size;
                    view.pagination.pages = Math.ceil(view.pagination.total / view.pagination.size);

                    // For demo purposes cap paging at 10 pages.
                    view.pagination.pages = (view.pagination.pages > 10) ? 10 : view.pagination.pages;
            	});
            };

            // Update field to sort by
            view.sort = function(field) {
                console.log(field);
                view.orderType[field] = !view.orderType[field];
                view.orderField = field;

                console.log(view.orderType);
            };

            // Generate array containing num elements
            view.getNumber = function(num) {
                var arr = [];
                for(var i = 0; i < num; i ++) {
                    arr.push(i);
                }
                return arr;
            };
        }
    };
}]);
